package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;
	private static int counter = 0;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean equals(Object obj){
		if(Objects.equals(this.login, obj.toString())){
			return true;
		}
		return false;
	}

	public User(){}

	User(String login){
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public String toString(){
		return login;
	}

}