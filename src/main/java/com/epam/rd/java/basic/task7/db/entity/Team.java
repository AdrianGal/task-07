package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;
	private static int counter = 0;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object obj){
		if(Objects.equals(this.name, obj.toString())){
			return true;
		}
		return false;
	}

	public Team(){}

	Team(String name){
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	public String toString(){
		return name;
	}

}