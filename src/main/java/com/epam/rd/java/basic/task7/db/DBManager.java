package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {


    public static final String CONNECTION_PROPERTIES_FILE_NAME = "app.properties";
    public static final String CONNECTION_URL_PROPERTY_NAME = "connection.url";

    private static String URL;

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }

        return instance;
    }

    private DBManager() {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(CONNECTION_PROPERTIES_FILE_NAME)) {
            properties.load(reader);
            URL = properties.getProperty(CONNECTION_URL_PROPERTY_NAME);

        } catch (IOException e) {
            e.printStackTrace();
            new IllegalStateException(e);
        }
    }

    private Connection getConnection(boolean autocommit) throws SQLException {
        Connection con = DriverManager.getConnection(URL);
        con.setAutoCommit(autocommit);
        con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return con;
    }


    public List<User> findAllUsers() throws DBException {
        try (Connection connection = getConnection(true);
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery("select * from users order by id")) {
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get users", e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = getConnection(true);
             PreparedStatement ps = connection.prepareStatement("INSERT INTO users (id,login) VALUES (default ,?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getLogin());
            int i = ps.executeUpdate();
            if (i > 0) {

                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(1));
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // log
            throw new DBException("Can not insert user: " + user, e);
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection connection = getConnection(true);
             PreparedStatement st = connection.prepareStatement("DELETE FROM test2db.users WHERE login=?")) {
            for (int i = 0; i < users.length; i++) {
                st.setString(1, users[i].getLogin());
                st.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot delete user", e);
        }
        return true;
    }

    static String escapeForLike(String param) {
        return param.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");
    }

    public User getUser(String login) throws DBException {
        try (Connection connection = getConnection(true);
             PreparedStatement st = connection.prepareStatement("SELECT * FROM users WHERE login LIKE ? ORDER BY id")) {
            st.setString(1, "%" + (login) + "%");
            try (ResultSet rs = st.executeQuery()) {
                User user = new User();
                if (rs.next()) {
                    user.setLogin(rs.getString("login"));
                    user.setId(rs.getInt("id"));
                }
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getUser", e);
        }
    }

    public Team getTeam(String name) throws DBException {
        try (Connection connection = getConnection(true);
             PreparedStatement st = connection.prepareStatement("SELECT * FROM teams WHERE name LIKE ? ORDER BY id")) {
            st.setString(1, "%" + escapeForLike(name) + "%");
            try (ResultSet rs = st.executeQuery();) {
                Team team = new Team();
                if (rs.next()) {
                    team.setName(rs.getString("name"));
                    team.setId(rs.getInt("id"));
                }
                return team;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get team", e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection connection = getConnection(true);
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery("select * from teams order by id");) {
            List<Team> teams = new ArrayList<>();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot find all teams", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {

        try (Connection connection = getConnection(true);
             PreparedStatement ps = connection.prepareStatement("INSERT INTO teams (id,name) VALUES (default ,?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, team.getName());
            int i = ps.executeUpdate();
            if (i > 0) {
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        team.setId(rs.getInt(1));
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // log
            throw new DBException("Good explanation of error", e);
        }

        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {

        Connection con = null;
        try {
            con = getConnection(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            for (Team team : teams) {
                setTFU(con, user.getId(), team.getId());
            }
            con.commit();

        } catch (SQLException e) {
            try {
                rollback(con);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            throw new DBException("Cannot set team for user", e);
        } finally {
            try {
                close(con);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;

    }

    static void rollback(Connection con) throws SQLException {
        if (con != null) {
            con.rollback();
        }
    }

    static void close(Connection con) throws SQLException {
        if (con != null) {
            con.close();
        }
    }

    public void setTFU(Connection connect, int userId, int teamId) throws SQLException {
        try (PreparedStatement ps = connect.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)")) {
            ps.setInt(1, userId);
            ps.setInt(2, teamId);
            ps.executeUpdate();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> result = new ArrayList<>();
        int userID = getUser(user.getLogin()).getId();
        try (Connection connection = getConnection(true)) {
            Statement st = connection.createStatement();
            String query = "SELECT * FROM teams INNER JOIN users_teams ON users_teams.team_id = teams.id WHERE users_teams.user_id =" + userID;
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                Team t = new Team();
                t.setName(rs.getString("name"));
                t.setId(rs.getInt("id"));
                result.add(t);
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getUserTeams", e.getCause());
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        int teamId = getTeam(team.getName()).getId();
        String inName = team.getName();
        try (Connection connection = getConnection(true);
             PreparedStatement st = connection.prepareStatement("DELETE FROM teams WHERE name='" + inName + "'")) {
            st.executeUpdate();
            PreparedStatement st2 = connection.prepareStatement("DELETE FROM users_teams WHERE team_id=" + teamId);
            st2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("deleteTeam", e);
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection connection;
        try {
            connection = getConnection(true);
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT id FROM teams WHERE name='" + team.getName() + "'");
            int id = 0;
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            st.close();
            PreparedStatement ps = connection.prepareStatement("UPDATE teams SET name='" + team.getName() + "' WHERE id=" + id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("updateTeam", e.getCause());
        }
        return false;
    }
}